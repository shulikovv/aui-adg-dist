const ONE_WEEK = 604800000;
const NOOP = () => {};

export const BASE_URL = '/atlaskit/registry';
export const STORY_BASE_URL = 'https://aui-cdn.atlassian.com/atlaskit/stories';

export const TABS = [
  {
    title: 'All components',
    filter: () => true,
  },
  {
    title: 'React',
    filter: component => component.platform === 'React',
    selected: true,
  },
  {
    title: 'ak-*',
    filter: component => component.name.indexOf('ak-') === 0,
  },
  {
    title: 'ak-editor-*',
    filter: component => component.name.indexOf('ak-editor-') === 0,
  },
  {
    title: 'akutil-*',
    filter: component => component.name.indexOf('akutil-') === 0,
  },
  {
    title: 'pf-*',
    filter: component => component.name.indexOf('pf-') === 0,
  },
  {
    title: 'eslint-*',
    filter: component => component.name.indexOf('eslint-') === 0,
  },
  {
    title: 'Recently updated',
    filter: component => component.publishTimestamp > Date.now() - ONE_WEEK,
    sort: (a, b) => b.publishTimestamp - a.publishTimestamp,
  },
].map((tab) => {
  tab.sort = tab.sort || NOOP;
  return tab;
});
