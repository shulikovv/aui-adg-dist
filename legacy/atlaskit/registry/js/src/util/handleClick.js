function hrefForEvent(e) {
  let finalTarget;

  if (e.target.href) {
    finalTarget = e.target;
  }

  let loopTarget = e.target;
  while (loopTarget.parentElement) {
    if (loopTarget.href) {
      finalTarget = loopTarget;
      break;
    }
    loopTarget = loopTarget.parentElement;
  }

  if (!finalTarget) {
    const path = e.path || e.composedPath();
    finalTarget = path.find(target => !!target.href);
  }

  return finalTarget && finalTarget.href;
}

export default function (elem) {
  return (e) => {
    // do nothing on middle click on cmd-click
    if (e.button === 1 || e.metaKey === true) return;

    const url = hrefForEvent(e);
    if (url) {
      const isExternal = url.indexOf(document.location.hostname) < 0 && url.indexOf('/') !== 0;
      if (!isExternal) {
        e.preventDefault();
        history.pushState(null, null, url);
        elem.rerenderHack += 1;
      }
    }
  };
}
