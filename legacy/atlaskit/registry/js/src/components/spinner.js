/* eslint-disable react/no-unknown-property */

import {
  define,
  h, // eslint-disable-line no-unused-vars
} from 'skatejs';

const size = 80;

const sharedStyles = `
@-moz-keyframes l8 {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
}
@-webkit-keyframes l8 {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
}
@-o-keyframes l8 {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
}
@keyframes l8 {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
}
@-moz-keyframes spinnerLoad2 {
  0% {
    transform: rotate(50deg);
  }
  100% {
    transform: rotate(230deg);
  }
}
@-webkit-keyframes spinnerLoad2 {
  0% {
    transform: rotate(50deg);
  }
  100% {
    transform: rotate(230deg);
  }
}
@-o-keyframes spinnerLoad2 {
  0% {
    transform: rotate(50deg);
  }
  100% {
    transform: rotate(230deg);
  }
}
@keyframes spinnerLoad2 {
  0% {
    transform: rotate(50deg);
  }
  100% {
    transform: rotate(230deg);
  }
}
@-moz-keyframes spinnerLoad {
  0% {
    transform: rotate(230deg);
  }
  100% {
    transform: rotate(510deg);
  }
}
@-webkit-keyframes spinnerLoad {
  0% {
    transform: rotate(230deg);
  }
  100% {
    transform: rotate(510deg);
  }
}
@-o-keyframes spinnerLoad {
  0% {
    transform: rotate(230deg);
  }
  100% {
    transform: rotate(510deg);
  }
}
@keyframes spinnerLoad {
  0% {
    transform: rotate(230deg);
  }
  100% {
    transform: rotate(510deg);
  }
}

.svg {
  position: relative;
  width: 520px;
  height: ${size}px;
  min-height: ${size}px;
  display: flex;
}

.spinner {
  animation: c1 1.6s cubic-bezier(0.47, 0.17, 0.6, 0.83) infinite;
}
.path-animate {
  stroke-dasharray: 60;
  stroke-dashoffset: 0;
  transform-origin: center;
  stroke: #42526e;
  animation: optimisticSlide 0.2s ease-in-out infinite;
}
#s8 {
  animation: spinnerLoad 0.53s ease-in-out;
  animation-fill-mode: forwards;
}
#s8 #s8inner .path-animate {
  stroke-dashoffset: 60;
  animation: l8 0.86s infinite;
  animation-timing-function: cubic-bezier(0.4, 0.15, 0.6, 0.85);
  transition: stroke-dashoffset 0.8s ease-in-out, opacity 0.2s ease-in-out 0.45s;
  opacity: 0;
}
#s8.active {
  animation: spinnerLoad2 1s ease-in-out;
  animation-fill-mode: forwards;
}
#s8.active #s8inner .path-animate {
  stroke-dashoffset: 50;
  opacity: 1;
  transition: stroke-dashoffset 0.8s ease-in-out, opacity 0.2s ease-in-out;
}
@-moz-keyframes c1 {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
}
@-webkit-keyframes c1 {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
}
@-o-keyframes c1 {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
}
@keyframes c1 {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
}
`;

export default define('atlaskit-registry-spinner', {
  render() {
    return (
      <div class="atlaskit-registry-spinner">
        <style>{sharedStyles}</style>
        <div class="svg">
          <div class="spinner active" id="s8">
            <svg
              id="s8inner"
              width={`${size}px`}
              height={`${size}px`}
              viewBox="0 0 30 30"
              xmlns="http://www.w3.org/2000/svg"
            >
              <circle
                class="path-animate"
                fill="none"
                stroke-width="1.5"
                stroke-linecap="round"
                cx="15"
                cy="15"
                r="7"
              />
            </svg>
          </div>
        </div>
      </div>
    );
  },
});
