import { define, prop, props, h } from 'skatejs'; // eslint-disable-line
import { BASE_URL } from '../config';
import Sparkline from '../components/sparkline';
import TabContent from '../components/tab_content';

const tableStyle = require('!css!less!../css/table.less'); // eslint-disable-line
const typeStyle = require('!css!less!../css/type.less'); // eslint-disable-line

export default define('akr-page-component-metrics', {
  props: {
    components: prop.array(),
  },
  attached(elem) {
    fetch('/atlaskit/registry/spa_data/metrics_page.json')
      .then(result => result.json())
      .then((json) => {
        elem.components = json;
      });
  },
  render(elem) {
    const { components } = elem;

    const header = (
      <div class="row table-row" style={{ 'font-weight': 'bold', 'margin-top': '20px' }}>
        <div class="col-3">Component</div>
        <div class="col-2">CSS size</div>
        <div class="col-2">CSS specificity (avg)</div>
        <div class="col-2">JS size</div>
      </div>
    );

    function row(component) {
      const stats = [
        {
          statGroup: 'cssStats',
          stat: 'size',
          suffix: ' b',
        },
        {
          statGroup: 'cssStats',
          stat: 'averageSelectorSpecificity',
        },
        {
          statGroup: 'jsStats',
          stat: 'size',
          suffix: ' b',
        },
      ];
      return (
        <div class="row table-row">
          <div class="col-3">
            <a href={`${BASE_URL}/${component.name}/latest/index.html`}>{component.name}</a><br />
          </div>
          {
            stats.map(stat => (
              <div>
                <div class="col-1">
                  {component[stat.statGroup][stat.stat] || 0}
                  {stat.suffix}
                  &nbsp;
                </div>
                <div class="col-1">
                  <Sparkline
                    reverse
                    values={component[stat.statGroup].sparkline[stat.stat]}
                  />
                </div>
              </div>
            ))
          }
        </div>
      );
    }

    return (
      <div>
        <style>
          {tableStyle.toString()}
          {typeStyle.toString()}
        </style>
        <h2>Metrics</h2>
        <TabContent
          components={components}
          header={header}
          rowFn={row}
        />
      </div>
    );
  },
});
