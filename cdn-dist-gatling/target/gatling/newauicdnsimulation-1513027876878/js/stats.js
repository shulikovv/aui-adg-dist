var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "1200",
        "ok": "1200",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "327",
        "ok": "327",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3511",
        "ok": "3511",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "659",
        "ok": "659",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "283",
        "ok": "283",
        "ko": "-"
    },
    "percentiles1": {
        "total": "580",
        "ok": "580",
        "ko": "-"
    },
    "percentiles2": {
        "total": "728",
        "ok": "728",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1208",
        "ok": "1208",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1713",
        "ok": "1713",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 973,
        "percentage": 81
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 166,
        "percentage": 14
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 61,
        "percentage": 5
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "9.756",
        "ok": "9.756",
        "ko": "-"
    }
},
contents: {
"req_get-js-aui-min--70d8c": {
        type: "REQUEST",
        name: "Get js/aui.min.js",
path: "Get js/aui.min.js",
pathFormatted: "req_get-js-aui-min--70d8c",
stats: {
    "name": "Get js/aui.min.js",
    "numberOfRequests": {
        "total": "1200",
        "ok": "1200",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "327",
        "ok": "327",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3511",
        "ok": "3511",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "659",
        "ok": "659",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "283",
        "ok": "283",
        "ko": "-"
    },
    "percentiles1": {
        "total": "580",
        "ok": "580",
        "ko": "-"
    },
    "percentiles2": {
        "total": "728",
        "ok": "728",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1208",
        "ok": "1208",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1713",
        "ok": "1713",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 973,
        "percentage": 81
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 166,
        "percentage": 14
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 61,
        "percentage": 5
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "9.756",
        "ok": "9.756",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
