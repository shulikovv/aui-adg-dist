#!/usr/bin/env bash

# Due to the large number of files when deploying all the existing distributions this script differs
# from deploy.sh in that it doesn't invalidate the cdn.

# print commands (-x) and stop execution on error (-e)
set -xe

if [ -z "$1" ]; then
    echo "Usage: $0 [dev-west2|stg-west2|prod-west2]"
    exit 1
fi

# Make micros cli available
npm i -g @atlassian/micros-cli

micros static:deploy aui-cdn -e $1 -f aui-cdn.sd.yaml