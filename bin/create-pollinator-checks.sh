#!/bin/bash

# A script to create pollinator checks for all of the AUI ADG versions we want to monitor
# You need to have installed the Pollinator CLI (https://extranet.atlassian.com/display/OBSERVABILITY/How+To%3A+Pollinator+CLI#installation--421128575)
# and have performed pollidev login first

# print commands (-x) and stop execution on error (-e)
set -xe

# All 67 versions of AUI-ADG that were still being accessed as of October 2017
versions=(
    "6.0.9"
    "6.0.8"
    "6.0.7"
    "6.0.6"
    "6.0.5"
    "6.0.4"
    "6.0.3"
    "6.0.2"
    "6.0.1"
    "6.0.0"
    "5.9.8"
    "5.9.7"
    "5.9.6"
    "5.9.5"
    "5.9.4"
    "5.9.3"
    "5.9.24"
    "5.9.22"
    "5.9.21"
    "5.9.2"
    "5.9.19"
    "5.9.18"
    "5.9.17"
    "5.9.16"
    "5.9.15"
    "5.9.14"
    "5.9.13"
    "5.9.12"
    "5.9.1"
    "5.9.0"
    "5.8.9"
    "5.8.8"
    "5.8.7"
    "5.8.4"
    "5.8.3"
    "5.8.20"
    "5.8.18"
    "5.8.15"
    "5.8.14"
    "5.8.13"
    "5.8.12"
    "5.8.11"
    "5.8.10"
    "5.8.1"
    "5.8.0"
    "5.7.9"
    "5.7.8"
    "5.7.5"
    "5.7.45"
    "5.7.31"
    "5.7.3"
    "5.7.27"
    "5.7.20"
    "5.7.18"    
    "5.7.16"
    "5.7.14"
    "5.7.12"
    "5.7.11"
    "5.7.1"
    "5.7.0"
    "5.6.8"
    "5.6.7"
    "5.6.16"
    "5.6.12"
    "5.6.11"
    "5.6.10"
    "5.10.1"
)

# For each version replace the ${version} in the template and create on Pollinator
for ver in ${versions[*]};
    do
        sed -e "s/\${version}/$ver/" ../monitoring/pollinator-asset-check-template.yml | pollidev create --interactive=false --no-save -
    done